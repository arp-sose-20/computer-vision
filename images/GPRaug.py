# -*- coding: utf-8 -*-
"""
Created on Fri May 29 20:35:56 2020

@author: schim
"""

from augmentation_random_cropping import augmentation_random_cropping
from image_augmentation import image_augmentation



augmentation_random_cropping([416,640,832],10,416,'image_train/','labelbox/','train','train_yolo/','D:/Users/ARP/images/imagestrain/',box=True,point_= True)       
      
image_augmentation('train_yolo/', 'imagestrain/', 'train_box.txt', flip=False, brightness=True, brightness_value=3.5, noise=True, noise_value=(10,80))
