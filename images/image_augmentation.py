
"""
Created on Wed May 27 09:20:58 2020

@author: Rosario
"""

import imageio
import imgaug as ia
import imgaug.augmenters as iaa
import cv2
import os
from os import listdir

def image_augmentation(image_path_in, image_path_out, labelfile_path, flip=True, brightness=True, brightness_value=3.5, noise=True, noise_value=(10,80)):
    #image_path_in = logisch oder?
    #image_path_out = logisch oder?
    #flip = flippen ja oder nein
    #brightness = Helligkeit ändern ja oder nein
    #brightness_value = gamma Faktor, je höher, desto dunkler
    #noise = gausnoise auf Bild legen ja oder nein
    #noise_value = (Mittelwert, Abweichung)
 
    
    #kleine Funktion, um nur die jpegs zu finden
    def list_files1(directory, extension):
        return (f for f in listdir(directory) if f.endswith('.' + extension))
    
    if not os.path.exists(image_path_out):#Guck ob das Zielverzeichnis existiert
        os.makedirs(image_path_out)#Wenn nicht, erstell es
        
    if not os.path.exists(image_path_out+'labels'):#Guck ob das Zielverzeichnis existiert
        os.makedirs(image_path_out+'labels')#Wenn nicht, erstell es
    file = open(image_path_out+'labels/labels_augmented.txt','w') #öffne, erstelle neue labelfile im Zielordner

    #"kopiere" alle Bilder aus Quell ins Zielverzeichnis
    for images in list_files1(image_path_in,'jpeg'):
        image = imageio.imread(image_path_in+images)#einlesen des Bildes
        #ia.imshow(image)#zeig mal das Bild
        cv2.imwrite(image_path_out+images,image)#Speichern
        with open(labelfile_path) as labels:#zieh dir die labels rein
                #Labelshittyfuck
                AllLabelString = labels.readlines()#speicher die in einer Liste
                AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
                TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spzifischem Bild
                if TheLabelString:
                    new_image_label_path = str(TheLabelString)[2:str(TheLabelString).find(" ")-len(images)]+images#machts einfacher
                    file.write(new_image_label_path+str(TheLabelString)[str(TheLabelString).find(" "):-2]+'\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile

    #Augmente alle vorhandenen Bilder im Zielverzeichnis
    #change brightness
    if brightness == True:
        for images in list_files1(image_path_in,'jpeg'):
            with open(labelfile_path) as labels:#zieh dir die labels rein
                #Labelshittyfuck
                AllLabelString = labels.readlines()#speicher die in einer Liste
                AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
                TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spzifischem Bild
                if TheLabelString:
                    new_image_label_path = str(TheLabelString)[2:str(TheLabelString).find(" ")-len(images)]+'b_'+images#machts einfacher
                    file.write(new_image_label_path+str(TheLabelString)[str(TheLabelString).find(" "):-2]+'\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile
                #Augmentation            
                image = imageio.imread(image_path_in+images)#einlesen des Bildes
                contrast=iaa.GammaContrast(gamma=brightness_value)
                contrast_image =contrast.augment_image(image)
                #ia.imshow(contrast_image)#zeig mal das Bild
                cv2.imwrite(image_path_out+'b_'+images,contrast_image)#Speichern
    
    #Adding noise
    if noise == True:
        for images in list_files1(image_path_in,'jpeg'):
            with open(labelfile_path) as labels:#zieh dir die labels rein
                #Labelshittyfuck
                AllLabelString = labels.readlines()#speicher die in einer Liste
                AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
                TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spzifischem Bild
                if TheLabelString:
                    new_image_label_path = str(TheLabelString)[2:str(TheLabelString).find(" ")-len(images)]+'n_'+images#machts einfacher
                    file.write(new_image_label_path+str(TheLabelString)[str(TheLabelString).find(" "):-2]+'\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile
                #Augmentation           
                image = imageio.imread(image_path_in+images)#einlesen des Bildes
                gaussian_noise=iaa.AdditiveGaussianNoise(noise_value)
                noise_image=gaussian_noise.augment_image(image)
                #ia.imshow(noise_image)#zeig mal das Bild
                cv2.imwrite(image_path_out+'n_'+images,noise_image)#Speichern

    #Augmente alle vorhandenen Bilder im Zielverzeichnis
    #flipping image horizontally
    if flip == True:
        for images in list_files1(image_path_in,'jpeg'):
            with open(labelfile_path) as labels:#zieh dir die labels rein
                #Labelshittyfuck
                AllLabelString = labels.readlines()#speicher die in einer Liste
                AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
                TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spezifischem Bild
                if TheLabelString:
                    new_image_label_path = str(TheLabelString)[2:str(TheLabelString).find(" ")-len(images)]+'f_'+images#machts einfacher
                    file.write(new_image_label_path)
                    file.write(' ')
                    for i in range(0,len(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" "))):
                        for j in range(0,len(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(","))):
                            if j is 0:
                                flipped_x = image.shape[1]-int(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j])
                                file.write(str(flipped_x)+",")
                            if j is 2:
                                flipped_x = image.shape[1]-int(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j])
                                file.write(str(flipped_x)+",")
                            if j is 4:
                                file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+" ")
                            if j is 1:
                                file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+",")
                            if j is 3:
                                file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+",")
                file.write('\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile
                #Augmentation           
                image = imageio.imread(image_path_in+images)#einlesen des Bildes
                flip_hr=iaa.Fliplr(p=1.0)
                flip_hr_image= flip_hr.augment_image(image)
                #ia.imshow(flip_hr_image)#zeig mal das Bild
                cv2.imwrite(image_path_out+'f_'+images,flip_hr_image)#Speichern
        if brightness == True:
            for images in list_files1(image_path_in,'jpeg'):
               with open(labelfile_path) as labels:#zieh dir die labels rein
                    #Labelshittyfuck
                    AllLabelString = labels.readlines()#speicher die in einer Liste
                    AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
                    TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spezifischem Bild
                    if TheLabelString:
                        new_image_label_path = str(TheLabelString)[2:str(TheLabelString).find(" ")-len(images)]+'bf_'+images#machts einfacher
                        file.write(new_image_label_path)
                        file.write(' ')
                        for i in range(0,len(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" "))):
                            for j in range(0,len(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(","))):
                                if j is 0:
                                    flipped_x = image.shape[1]-int(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j])
                                    file.write(str(flipped_x)+",")
                                if j is 2:
                                    flipped_x = image.shape[1]-int(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j])
                                    file.write(str(flipped_x)+",")
                                if j is 4:
                                    file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+" ")
                                if j is 1:
                                    file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+",")
                                if j is 3:
                                    file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+",")
                    file.write('\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile
                    #Augmentation           
                    image = imageio.imread(image_path_in+images)#einlesen des Bildes
                    contrast=iaa.GammaContrast(gamma=brightness_value)
                    contrast_image =contrast.augment_image(image)
                    flip_hr=iaa.Fliplr(p=1.0)
                    flip_hr_image= flip_hr.augment_image(contrast_image)
                    #ia.imshow(flip_hr_image)#zeig mal das Bild
                    cv2.imwrite(image_path_out+'bf_'+images,flip_hr_image)#Speichern
        if noise == True:
            for images in list_files1(image_path_in,'jpeg'):
                with open(labelfile_path) as labels:#zieh dir die labels rein
                    #Labelshittyfuck
                    AllLabelString = labels.readlines()#speicher die in einer Liste
                    AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
                    TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spezifischem Bild
                    if TheLabelString:
                        new_image_label_path = str(TheLabelString)[2:str(TheLabelString).find(" ")-len(images)]+'fn_'+images#machts einfacher
                        file.write(new_image_label_path)
                        file.write(' ')
                        for i in range(0,len(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" "))):
                            for j in range(0,len(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(","))):
                                if j is 0:
                                    flipped_x = image.shape[1]-int(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j])
                                    file.write(str(flipped_x)+",")
                                if j is 2:
                                    flipped_x = image.shape[1]-int(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j])
                                    file.write(str(flipped_x)+",")
                                if j is 4:
                                    file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+" ")
                                if j is 1:
                                    file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+",")
                                if j is 3:
                                    file.write(str(TheLabelString)[str(TheLabelString).find(" ")+1:-2].split(" ")[i].split(",")[j]+",")
                    file.write('\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile
                    #Augmentation           
                    image = imageio.imread(image_path_in+images)#einlesen des Bildes
                    gaussian_noise=iaa.AdditiveGaussianNoise(noise_value)
                    noise_image=gaussian_noise.augment_image(image)
                    flip_hr=iaa.Fliplr(p=1.0)
                    flip_hr_image= flip_hr.augment_image(noise_image)
                    #ia.imshow(flip_hr_image)#zeig mal das Bild
                    cv2.imwrite(image_path_out+'fn_'+images,flip_hr_image)#Speichern
                
    file.close()

    
     

#Des war noch zum testen
# image_path_in = 'crop_img/'
# image_path_out = 'augmented/'
# labelfile_path = '4_CLASS_test.txt'

# image_augmentation(image_path_in, image_path_out,labelfile_path)
'''
for images in os.listdir(image_path_in):
    new_image_path = image_path_out+'f_'+images#machts einfacher
    with open(labelfile_path) as labels:#zieh dir die labels rein
        #Labelshittyfuck
        AllLabelString = labels.readlines()#speicher die in einer Liste
        AllLabelString = [x.strip() for x in AllLabelString]#mach se schick
        TheLabelString = [s for s in AllLabelString if images in s]#such nach dem spzifischem Bild

        #if TheLabelString:
        #    file.write(new_image_path+str(TheLabelString)[str(TheLabelString).find(" "):-2]+'\n')#nimm den neuen Path und die Labels und schireb sie in die neue labelfile
        #Augmentation           
        image = imageio.imread(image_path_out+images)#einlesen des Bildes
        #flip_hr=iaa.Fliplr(p=1.0)
        #flip_hr_image= flip_hr.augment_image(image)
        with_l = cv2.rectangle(image,(image.shape[1]-0,657),(image.shape[1]-300,1000),(0,0,255),15)  
        ia.imshow(with_l)#zeig mal das Bild
        #cv2.imwrite(new_image_path,flip_hr_image)#Speichern
        
'''
