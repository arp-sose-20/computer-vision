# # -*- coding: utf-8 -*-
# """
# Created on Fri May 29 07:20:02 2020

# @author: schim
# """



#augmentation_random_cropping(48 ,10,100,10,-10,10,-10,'image/','labelbox/','label','Test/','/floyd/home/images/',box=False)  
#augmentation_random_cropping(hälfe der Bildgröße, halbe größe von der Boundingbox, rezize bei 100 werden die Bilder nicht verkleinert, random faktor x max, random faktor x min,random faktor y max, random faktor y min, ordner wo die Bilder liege, Ordner wo die Labels von labelbox lieben, ordner wo die Bilder abgespeichert werden sollen, pfad für yolo, box = True erstellt Baunding Box, Box false erstellt punkt)

     

def augmentation_random_cropping (new_image_size,size_boundingbox,input_size_yolo,path_to_images, path_to_json,txt_filename,path_to_the_new_image,path_for_training_yolo,box=True,point_=True):
  import random 
  import json
  import cv2
  import os
  import glob
  
  
  print('GPR NET')
  random_cropping_size = {'very small':{'x':20,'y':20},
                         'small':{'x':150,'y':75},
                         'medium':{'x':150,'y':150},
                         'large':{'x':50,'y':10}}

  def listToString(inList):
        outString = ''
        if len(inList)==1:
            outString = outString+str(inList[0])
        if len(inList)>1:
            outString = outString+str(inList[0])
            for items in inList[1:]:
                outString = outString+' '+str(items)
        return outString
    
    
  if point_ == True: 
       file_point = open(txt_filename + '_point' + '.txt',"w") 
  if box == True:
        file_box = open(txt_filename + '_box' + '.txt',"w") 
  folder_images = list(filter(lambda x: os.path.isdir(os.path.join(path_to_images, x)), os.listdir(path_to_images)))
    

  for index_size, size in enumerate(new_image_size): 
      
    half_size_boundingbox = size_boundingbox/2 * size/input_size_yolo  
    scale_percent = input_size_yolo/size   
    labeling = []
   
    half_size_new_image = size/2
    
    if size < 100:
        random_x_ = random_cropping_size['very small']['x']
        random_y_ = random_cropping_size['very small']['y']    

    elif size > 100 and size < 500:
        random_x_ = random_cropping_size['small']['x']
        random_y_ = random_cropping_size['small']['y']
    elif size > 550 and size < 1000:
        random_x_ = random_cropping_size['medium']['x']
        random_y_ = random_cropping_size['medium']['y']
    elif size > 1000 and size < 1900:
        random_x_ = random_cropping_size['large']['x']
        random_y_ = random_cropping_size['large']['y']

        
        
        
    for file_name in [file for file in os.listdir(path_to_json) if file.endswith('.json')]:
      with open(path_to_json + file_name) as json_file:
        folder_labelbox = json.load(json_file)
        labeling.append(folder_labelbox)
   
    new_coordinates = {}
    new_coordinates_point = {}
    for index_labeling, image_categorie in enumerate(labeling): 
      images_in_folder=[]
      filenames = glob.glob(path_to_images+folder_images[index_labeling]+'/'+'*'+".jpeg")
      filenames.sort()
      for z in filenames:
          a = len(path_to_images)
          b= len(folder_images[index_labeling])
          images_in_folder.append(z[a+b+1:a+b+9])  
      for image in image_categorie:
        label_point=[]
        create_new_image = []
        for j in image['Label']['objects']:
                  if 'point' in j:
                      label_point.append(j['point'])
              
        #[create_new_image.append(label) for label in label_point if label['x'] > (half_size_new_image + random_x_max) and label['x'] < (1850 - half_size_new_image - random_x_max) and label['y'] > (half_size_new_image+randdom_y_max) and label['y'] < (1850-(half_size_new_image+randdom_y_max)) ] 
        [create_new_image.append(label) for label in label_point if label['x'] > (half_size_new_image + random_x_) and label['x'] < (1850 - half_size_new_image - random_x_) and label['y'] > (half_size_new_image + random_y_) and label['y'] < (1850-(half_size_new_image + random_y_)) ] 
         
        if image['External ID'][:8] in images_in_folder:
          imge = cv2.imread(path_to_images+folder_images[index_labeling]+'/'+image['External ID'][:8]+'.jpeg')
        
          for index_label, label in enumerate(create_new_image):
                labels_in_new_image = []
                
                x_min_box = []
                x_max_box = []
                y_min_box = []
                y_max_box = []
                y_point = []
                x_point = []
                
                random_x = random.randint(-random_x_, random_x_)
                random_y = random.randint(-random_y_, random_y_)
                x =  label['x']
                y =  label['y']
                x_mitte = x + random_x
                y_mitte = y + random_y
                x_1 = round(x_mitte - half_size_new_image)
                x_2 = round(x_mitte + half_size_new_image)
                y_1 = round(y_mitte - half_size_new_image)
                y_2 = round(y_mitte + half_size_new_image)
                img = imge[y_1:y_2,x_1:x_2]
       
               
     
                width = int(img.shape[1] * scale_percent )
                height = int(img.shape[0] * scale_percent)
                dim = (width, height)

                img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                for point in label_point:
                    if point['x'] > (x_1+half_size_boundingbox):
                        if point['x'] < (x_mitte+half_size_new_image-half_size_boundingbox):
                          labels_in_new_image.append(point)
                          y = point['y'] - y_1
                          y1 = y - half_size_boundingbox
                          y2 = y + half_size_boundingbox
                          x = point['x'] - x_1
                          x1 = x - half_size_boundingbox
                          x2 = x + half_size_boundingbox
                          if box == True:
                              x_min_box.append(round(x1 * scale_percent))
                              x_max_box.append(round(x2 * scale_percent))
                              y_min_box.append(round(y1 * scale_percent))
                              y_max_box.append(round(y2 * scale_percent))
                          if point_ == True:
                             y_point.append(y*scale_percent)
                             x_point.append(x*scale_percent)
                             
                if box == True:    
                    new_coordinates[image['External ID'][:8]+'_'+str(index_label)+'_'+str(index_size)] = x_min_box ,y_min_box,x_max_box,y_max_box 
                if point_ == True:    
                    new_coordinates_point[image['External ID'][:8]+'_'+str(index_label)+'_'+str(index_size)] =x_point ,y_point 
                cv2.imwrite(path_to_the_new_image + image['External ID'][:8]+'_'+str(index_label)+'_'+str(index_size)+'.jpeg',img)  
    
    print('cropping finish')            
    
    
    if box == True:
               
        for i in new_coordinates: 
            label_text = []
            imge = cv2.imread(path_to_the_new_image+i+'.jpeg')
            for j in new_coordinates[i][0]:
                x_min = j
                y_min = new_coordinates[i][1][new_coordinates[i][0].index(j)]
                x_max = new_coordinates[i][2][new_coordinates[i][0].index(j)]
                y_max = new_coordinates[i][3][new_coordinates[i][0].index(j)]
                cv2.rectangle(imge,(x_min,y_min),(x_max,y_max),color=(31, 33, 201), thickness=3) 
                label_text.append(str(x_min) + ','+ str(y_min) + ','+ str(x_max) + ',' + str(y_max)+',0')
            file_box.writelines(path_for_training_yolo+i+'.jpeg'+ ' '+listToString(label_text)+'\n')
            cv2.imwrite('aug/'+i+'.jpeg',imge)  
    if point_ == True: 
       
       for i in new_coordinates_point: 
            label_text = []
            
            for j in new_coordinates_point[i][0]:
                x = j
                y = new_coordinates_point[i][1][new_coordinates_point[i][0].index(j)]
                label_text.append(str(x) + ','+ str(y))
            file_point.writelines(path_for_training_yolo+i+'.jpeg'+ ' '+listToString(label_text)+'\n')
           
  
                   
   
             
                          
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      